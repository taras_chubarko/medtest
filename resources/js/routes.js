import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes:[
        {path: '/', name: 'index', component: () => import('../js/pages/PageIndex.vue')},
        {path: '/countries', name: 'countries', component: () => import('../js/pages/PageCountries.vue')},
        {path: '/countries/create', name: 'countries.create', component: () => import('../js/pages/PageCreateCountry.vue')},
        {path: '/countries/edit/:id', name: 'countries.edit', component: () => import('../js/pages/PageEditCountry.vue')},
    ]
});

export default router;
