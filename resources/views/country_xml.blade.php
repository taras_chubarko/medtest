<?php
echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<root>
    @if(is_object($data))
        @foreach($data as $datum)
            <element>
                <capital>{{$datum->capital}}</capital>
                <country>{{$datum->country}}</country>
            </element>
        @endforeach
    @else
        @foreach($data as $datum)
            <element>
                <capital>{{$datum['capital']}}</capital>
                <country>{{$datum['country']}}</country>
            </element>
        @endforeach
    @endif
</root>
