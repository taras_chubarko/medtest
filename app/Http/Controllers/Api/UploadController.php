<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    //
    /* public function upload
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function upload(Request $request)
    {
        $file = $request->file;

        $path = Storage::putFileAs(
            'import', $file, $file->getClientOriginalName()
        );
        return response()->json($path);
    }

    /* public function fileList
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function fileList(Request $request)
    {
        $files = Storage::allFiles('import');
        return response()->json($files);
    }

    /* public function removeFile
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function removeFile(Request $request)
    {
        Storage::delete($request->file);
        return response()->json(true);
    }
}
