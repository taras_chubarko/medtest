<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Csv\Writer;

class ExportController extends Controller
{
    //
    /* public function getFormats
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFormats()
    {
        $files = Storage::files('import');
        $data = [];
        foreach ($files as $file) {
            $data[] = pathinfo($file, PATHINFO_EXTENSION);
        }
        return response()->json($data);
    }

    /* public function exportTo
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function exportTo(Request $request)
    {
        $data = Country::all();
        Storage::makeDirectory('public/export');
        $file = $this->{'export' . strtoupper($request->format)}($data);
        return $file;
    }

    /* public function exportCSV
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function exportCSV($data)
    {
       $path = storage_path('app/public/export/export_country.csv');
        $data = $data->map(function ($v) {
            return $v->only(['country', 'capital']);
        });
        $header = ['country', 'capital'];

        $writer = Writer::createFromPath($path, 'w+');
        $writer->insertOne($header);
        $writer->insertAll($data->toArray());
        return url('/') . '/storage/export/export_country.csv';
    }

    /* public function exportJSON
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function exportJSON($data)
    {
        $path = 'public/export/export_country.json';
        $data = $data->map(function ($v) {
            return $v->only(['country', 'capital']);
        });
        Storage::put($path, $data);
        return url('/') . '/storage/export/export_country.json';
    }

    /* public function exportXML
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function exportXML($data)
    {
        $path = 'public/export/export_country.xml';
        $content = \View::make('country_xml')->with('data', $data)->render();
        Storage::put($path, $content);
        return url('/') . '/storage/export/export_country.xml';
    }


}
