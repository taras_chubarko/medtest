<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImportController extends Controller
{
    //
    /* public function import
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function import(Request $request)
    {
        $path = storage_path('app/' . $request->file);
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        //$file = Storage::get($request->file);
        switch ($ext) {
            case 'csv':
                $this->importCSV($path);
                break;
            case 'json':
                $this->importJSON($path);
                break;
            case 'xml':
                $this->importXML($path);
                break;
        }
        return response()->json(true);
    }

    /* public function readCSV
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function readCSV($csvFile, $array)
    {
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 0, $array['delimiter']);
        }
        fclose($file_handle);
        return $line_of_text;
    }

    /* public function importCSV
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function importCSV($file)
    {
        $arr = $this->readCSV($file, array('delimiter' => ','));

        foreach ($arr as $k => $item) {
            $d = [];
            if ($item && $k > 0) {
                $d['country'] = $item[0];
                $d['capital'] = $item[1];
                $this->createOrUpdate($d);
            }
        }
    }

    /* public function importJSON
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function importJSON($file)
    {
        $jsonString = file_get_contents($file);
        $data = json_decode($jsonString, true);
        foreach ($data as $datum) {
            $this->createOrUpdate($datum);
        }
    }

    /* public function importXML
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function importXML($file)
    {
        $xmlString = file_get_contents($file);
        $xmlObject = simplexml_load_string($xmlString);
        $json = json_encode($xmlObject);
        $data = json_decode($json, true);
        foreach ($data['element'] as $datum) {
            $this->createOrUpdate($datum);
        }
    }

    /* public function createOrUpdate
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function createOrUpdate($datum)
    {
        $item = Country::query()->where('country', $datum['country'])->where('capital', $datum['capital'])->first();
        if (!$item) {
            Country::query()->create($datum);
        } else {
            $item->update($datum);
        }
    }
}
