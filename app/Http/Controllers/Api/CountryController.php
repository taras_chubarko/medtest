<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    //
    /* public function index
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function index()
    {
        return response()->json(Country::all());
    }

    /* public function store
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function store(Request $request)
    {
        $item = Country::create($request->all());
        return response()->json($item);
    }

    /* public function show
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function show($id)
    {
        $item = Country::find($id)->only(['country', 'capital']);
        return response()->json($item);
    }

    /* public function update
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function update(Request $request, $id)
    {
        Country::where('id', $id)->update($request->all());
        $item = Country::find($id)->only(['country', 'capital']);
        return response()->json($item);
    }

    /* public function destroy
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function destroy($id)
    {
        Country::where('id', $id)->delete();
        return response()->json(true);
    }
}
