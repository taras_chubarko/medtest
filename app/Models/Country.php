<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $guarded = [
        '_method',
        '_token',
    ];
}
