<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use League\Csv\Writer;

class ConvertCountriesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:countries {--input-file=} {--output-file=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cover formats in country --input-file=countries.xml --output-file=countries.json';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Storage::makeDirectory('public/convert');
        $options = $this->options();
        if ($options['input-file'] && $options['output-file']) {
            $path = storage_path('app/import/' . $options['input-file']);
            if (file_exists($path)) {
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $readData = $this->{'read' . strtoupper($ext)}($path);
                $exp = explode('.', $options['output-file']);
                $this->{'convert' . strtoupper($exp[1])}($readData, $options['output-file']);
            } else {
                $this->error('No file in directory');
            }
        } else {
            $this->error('Enter input output file');
        }
    }

    /* public function readXML
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function readXML($path)
    {
        $xmlString = file_get_contents($path);
        $xmlObject = simplexml_load_string($xmlString);
        $json = json_encode($xmlObject);
        $data = json_decode($json, true);
        return $data['element'];
    }

    /* public function readCSV
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function readCSV($path)
    {
        $arr = $this->rCSV($path, array('delimiter' => ','));
        $items = [];
        foreach ($arr as $k => $item) {
            $d = [];
            if ($item && $k > 0) {
                $d['country'] = $item[0];
                $d['capital'] = $item[1];
                $items[] = $d;
            }
        }
        return $items;
    }

    /* public function readCSV
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function rCSV($csvFile, $array)
    {
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 0, $array['delimiter']);
        }
        fclose($file_handle);
        return $line_of_text;
    }

    /* public function readJSON
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function readJSON($path)
    {
        $jsonString = file_get_contents($path);
        $data = json_decode($jsonString, true);
        return $data;
    }

    /* public function convert
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function convertJSON($data, $filename)
    {
        $data = collect((object)$data);
        $path = 'public/convert/' . $filename;
        Storage::put($path, $data);
       $this->info('Converted to ' . $path);
    }

    /* public function convert
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function convertXML($data, $filename)
    {
       // $data = collect((object)$data);
        $path = 'public/convert/' . $filename;
        $content = \View::make('country_xml')->with('data', $data)->render();
        Storage::put($path, $content);
        $this->info('Converted to ' . $path);
    }

    /* public function convert
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function convertCSV($data, $filename)
    {
        // $data = collect((object)$data);
        $path = storage_path('app/public/convert/'.$filename);
        $header = ['country', 'capital'];

        $writer = Writer::createFromPath($path, 'w+');
        $writer->insertOne($header);
        $writer->insertAll($data);
        $this->info('Converted to ' . $path);
    }

}
